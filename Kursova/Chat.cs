﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.Threading;

namespace Kursova
{
    public partial class Chat : Form
    {
        Form1 Fr1;
        public Chat()
        {
            InitializeComponent();
        }

        private void button_main_send_Click(object sender, EventArgs e)
        {
            Fr1 = this.Owner as Form1;
            string line = "";
            line = Convert.ToString(textBox_line_main.Text);
            textBox_line_main.Clear();
            textBox_chat_main.AppendText("You: " + line + "\n");
            textBox_chat_tel.AppendText("Main: " + line + "\n");
        }

        private void button_tel_send_Click(object sender, EventArgs e)
        {
            Fr1 = this.Owner as Form1;
            string line = "";
            line = Convert.ToString(textBox_line_tel.Text);
            textBox_line_tel.Clear();
            textBox_chat_tel.AppendText("You : " + line + "\n");
            textBox_chat_main.AppendText("Telephone #" + Fr1.textBox_main.Text + ": " + line + "\n");
        }

        private void button_main_chat_down_Click(object sender, EventArgs e)
        {
            Fr1 = this.Owner as Form1;
            Audio.Play_Knopka();
            Thread.Sleep(500);
            if (Fr1.button_down_tel_1.Enabled == true)
            {
                Fr1.textBox_tel_1.Clear();
            }
            if (Fr1.button_down_tel_2.Enabled == true)
            {
                Fr1.textBox_tel_2.Clear();
            }
            if (Fr1.button_down_tel_3.Enabled == true)
            {
                Fr1.textBox_tel_3.Clear();
            }
            if (Fr1.button_down_tel_4.Enabled == true)
            {
                Fr1.textBox_tel_4.Clear();
            }
            Abonent.Activation_button_of_tel_number(Fr1, Mini_ATC.Last_number);
            Mini_ATC.Activate_all_button_of_MiniATC(Fr1);
            Close();
        }

        private void button_tel_chat_down_Click(object sender, EventArgs e)
        {
            Fr1 = this.Owner as Form1;
            Audio.Play_Knopka();
            Thread.Sleep(500);
            if (Fr1.button_down_tel_1.Enabled == true)
            {
                Fr1.textBox_tel_1.Clear();
            }
            if (Fr1.button_down_tel_2.Enabled == true)
            {
                Fr1.textBox_tel_2.Clear();
            }
            if (Fr1.button_down_tel_3.Enabled == true)
            {
                Fr1.textBox_tel_3.Clear();
            }
            if (Fr1.button_down_tel_4.Enabled == true)
            {
                Fr1.textBox_tel_4.Clear();
            }
            Mini_ATC.Down_handset();
            Thread.Sleep(4000);
            Abonent.Activation_button_of_tel_number(Fr1, Mini_ATC.Last_number);
            Mini_ATC.Activate_all_button_of_MiniATC(Fr1);
            Close();
        }
    }
}
