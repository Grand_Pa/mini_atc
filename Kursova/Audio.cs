﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;

namespace Kursova
{
    static class Audio
    {
        public static void Play_Zanyato()
        {
            SoundPlayer call = new SoundPlayer(Properties.Resources.Zanyato);
            call.Stop();
            call.Play();
        }

        public static void Play_Viklik()
        {
            SoundPlayer call = new SoundPlayer(Properties.Resources.Viklik);
            call.PlayLooping();
        }

        public static void Play_Trubka()
        {
            SoundPlayer call = new SoundPlayer(Properties.Resources.Trubka);
            call.Stop();
            call.PlayLooping();
        }

        public static void Play_Knopka()
        {
            SoundPlayer call = new SoundPlayer(Properties.Resources.Knokpka);
            call.Play();
        }
    }
}
