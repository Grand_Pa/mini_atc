﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;

namespace Kursova
{
    class Phone
    {
        int number;

        public Phone(int number_)
        {
            Number = number_;
        }
        public int Number
        {
            get
            {
                return number;
            }
            set
            {
                number = value;
            }
        }

    }
}
