﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Windows.Forms;
using System.Threading;

namespace Kursova
{
    static class Mini_ATC
    {
        public static List<Phone> list_number = new List<Phone>();
        public static int last_number=0;

        public static int Last_number
        {
            get
            {
                return last_number;
            }
            set
            {
                last_number = value;
            }
        }

        public static void Write_number_in_list()
        {
            int number;
            Random rand = new Random();
            for(int i=0;i<4;i++)
            {
                number = rand.Next(100, 999);
                Phone phone_phone = new Phone(number);
                list_number.Add(phone_phone);
            }
        }
        public static void Up_handset()
        {
            Audio.Play_Trubka();
        }

        public static string Read_number_from_list()
        {
            int a = 0;
            string line = "";
            foreach(var Ar in list_number)
            {
                a++;
                if(Ar == null)
                {
                    line = line + "List of numbers is empty !";
                    return line;
                }

                Phone Ph = Ar as Phone;

                if ((object)Ph == null)
                {
                    line = line + "List of numbers is empty !";
                    return line;
                }
                line = line + "Telephone " + a + ": " + "#" + Ph.Number + "\n";
            }
            return line;
        }


        public static void Activate_all_button_of_MiniATC(object obj)
        {
            Form1 F1 = obj as Form1;
            bool position = (!F1.button_main_1.Enabled);
            if(F1.button_main_1.Enabled == false)
            {
                Up_handset();
            }
            F1.button_main_1.Enabled = position;
            F1.button_main_2.Enabled = position;
            F1.button_main_3.Enabled = position;
            F1.button_main_4.Enabled = position;
            F1.button_main_5.Enabled = position;
            F1.button_main_6.Enabled = position;
            F1.button_main_7.Enabled = position;
            F1.button_main_8.Enabled = position;
            F1.button_main_9.Enabled = position;
            F1.button_main_0.Enabled = position;
            F1.button_main_11.Enabled = position;
            F1.button_main_12.Enabled = position;
            F1.button_main_call.Enabled = position;
            F1.button_main_backspace.Enabled = position;
        }

        public static void type_number(object obj, string number)
        {
            Form1 F1 = obj as Form1;
            F1.textBox_main.AppendText(number);
        }

        public static bool check_number_in_list(object obj, ref int number_phone)
        {
            Form1 F1 = obj as Form1;
            int number;
            int a = 0;
            bool check = false;
            if (F1.textBox_main.Text == "#")
            {
                if (last_number != 0)
                {
                    number_phone = last_number;
                    check = true;
                }
                else
                {
                    MessageBox.Show("Last number is not present !", "Warning !", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    check = false;
                }
            }
            else
            {
                try
                {
                    number = Convert.ToInt32(F1.textBox_main.Text);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Invalid Input.", "Warning !", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Audio.Play_Trubka();
                    return false;
                }
                foreach (var Ar in list_number)
                {
                    a++;
                    if (Ar == null)
                    {
                        MessageBox.Show("List of numbers is empty !", "Warning !", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                    Phone Ph = Ar as Phone;
                    if (Ph.Number == number)
                    {
                        check = true;
                        number_phone = a;
                        last_number = a;
                        break;
                    }
                }
                if (check != true)
                {
                    MessageBox.Show("The numbers are not in the contact list", "Warning !", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return check;
        }

        public static void Delete_symvol(object obj)
        {
            Form1 F1 = obj as Form1;
            try
            { 
                F1.textBox_main.Text = F1.textBox_main.Text.Remove(F1.textBox_main.Text.Length - 1);
            }
            catch(Exception e)
            {
                MessageBox.Show("The enter list is empty !", "Warning !", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public static void Down_handset()
        {
            Audio.Play_Zanyato();
        }
    }
}
