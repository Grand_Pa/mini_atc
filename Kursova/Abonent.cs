﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursova
{
    static class Abonent
    {

        public static void Activation_button_of_tel_number(object obj, int number)
        {
            Form1 F1 = obj as Form1;
            bool check;
            switch (number)
            {
                case 1:
                    check = (!F1.button_down_tel_1.Enabled);
                    F1.button_down_tel_1.Enabled = check;
                    F1.button_call_tel_1.Enabled = check;
                    if (check == true)
                    {
                        Audio.Play_Viklik();
                        F1.textBox_tel_1.Text = "Call from main tel...";
                    }
                    break;
                case 2:
                    check = (!F1.button_down_tel_2.Enabled);
                    F1.button_down_tel_2.Enabled = check;
                    F1.button_call_tel_2.Enabled = check;
                    if (check == true)
                    {
                        Audio.Play_Viklik();
                        F1.textBox_tel_2.Text = "Call from main tel...";
                    }
                    break;
                case 3:
                    check = (!F1.button_down_tel_3.Enabled);
                    F1.button_down_tel_3.Enabled = check;
                    F1.button_call_tel_3.Enabled = check;
                    if (check == true)
                    {
                        Audio.Play_Viklik();
                        F1.textBox_tel_3.Text = "Call from main tel...";
                    }
                    break;
                case 4:
                    check = (!F1.button_down_tel_4.Enabled);
                    F1.button_down_tel_4.Enabled = check;
                    F1.button_call_tel_4.Enabled = check;
                    if (check == true)
                    {
                        Audio.Play_Viklik();
                        F1.textBox_tel_4.Text = "Call from main tel...";
                    }
                    break;
                default:
                    break;
            }
        }

    }
}
