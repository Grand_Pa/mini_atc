﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.Threading;

namespace Kursova
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Mini_ATC.Write_number_in_list();
        }

        public Chat Chat
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        private void button_main_down_Click(object sender, EventArgs e)
        {
            textBox_main.Clear();
            Audio.Play_Knopka();
            Thread.Sleep(500);
            if (button_down_tel_1.Enabled == true)
            {
                textBox_tel_1.Clear();
                Abonent.Activation_button_of_tel_number(this, Mini_ATC.Last_number);
            }
            if(button_down_tel_2.Enabled == true)
            {
                textBox_tel_2.Clear();
                Abonent.Activation_button_of_tel_number(this, Mini_ATC.Last_number);
            }
            if(button_down_tel_3.Enabled == true)
            {
                textBox_tel_3.Clear();
                Abonent.Activation_button_of_tel_number(this, Mini_ATC.Last_number);
            }
            if(button_down_tel_4.Enabled == true)
            {
                textBox_tel_4.Clear();
                Abonent.Activation_button_of_tel_number(this, Mini_ATC.Last_number);
            }
            //Mini_ATC.Up_handset();
            Mini_ATC.Activate_all_button_of_MiniATC(this);
        }

        private void button_main_cental_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Mini_ATC.Read_number_from_list(),"Contact list", MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
        }

        private void button_central_tel_2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Mini_ATC.Read_number_from_list(), "Contact list", MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
        }

        private void button_central_tel_1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Mini_ATC.Read_number_from_list(), "Contact list", MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
        }

        private void button_central_tel_4_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Mini_ATC.Read_number_from_list(), "Contact list", MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
        }

        private void button_central_tel_3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Mini_ATC.Read_number_from_list(), "Contact list", MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
        }

        private void button_main_1_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.type_number(this, "1");
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_main_2_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.type_number(this, "2");
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_main_3_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.type_number(this, "3");
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_main_4_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.type_number(this, "4");
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_main_5_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.type_number(this, "5");
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_main_6_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.type_number(this, "6");
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_main_7_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.type_number(this, "7");
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_main_8_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.type_number(this, "8");
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_main_9_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.type_number(this, "9");
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_main_0_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.type_number(this, "0");
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_main_12_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.type_number(this, "#");
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_main_11_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.type_number(this,"*");
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_main_call_Click(object sender, EventArgs e)
        {
            int number = 0;
            Audio.Play_Knopka();
            Thread.Sleep(500);
            if (Mini_ATC.check_number_in_list(this, ref number)) // дописать вывод чата
            {
                Abonent.Activation_button_of_tel_number(this, number);
                Mini_ATC.Activate_all_button_of_MiniATC(this);
            }
            else
            {
                Mini_ATC.Up_handset();
            }
        }

        private void button_main_backspace_Click(object sender, EventArgs e)
        {
            Audio.Play_Knopka();
            Mini_ATC.Delete_symvol(this);
            Thread.Sleep(500);
            Mini_ATC.Up_handset();
        }

        private void button_down_tel_1_Click(object sender, EventArgs e)
        {
            textBox_tel_1.Clear();
            Audio.Play_Knopka();
            Thread.Sleep(500);
            Mini_ATC.Down_handset();
            Thread.Sleep(4000);
            //Mini_ATC.Up_handset();
            Abonent.Activation_button_of_tel_number(this, Mini_ATC.Last_number);
            Mini_ATC.Activate_all_button_of_MiniATC(this);
        }

        private void button_down_tel_2_Click(object sender, EventArgs e)
        {
            textBox_tel_2.Clear();
            Audio.Play_Knopka();
            Thread.Sleep(500);
            Mini_ATC.Down_handset();
            Thread.Sleep(4000);
            //Mini_ATC.Up_handset();
            Abonent.Activation_button_of_tel_number(this, Mini_ATC.Last_number);
            Mini_ATC.Activate_all_button_of_MiniATC(this);
        }

        private void button_down_tel_3_Click(object sender, EventArgs e)
        {
            textBox_tel_3.Clear();
            Audio.Play_Knopka();
            Thread.Sleep(500);
            Mini_ATC.Down_handset();
            Thread.Sleep(4000);
            //Mini_ATC.Up_handset();
            Abonent.Activation_button_of_tel_number(this, Mini_ATC.Last_number);
            Mini_ATC.Activate_all_button_of_MiniATC(this);
        }

        private void button_down_tel_4_Click(object sender, EventArgs e)
        {
            textBox_tel_4.Clear();
            Audio.Play_Knopka();
            Thread.Sleep(500);
            Mini_ATC.Down_handset();
            Thread.Sleep(4000);
            //Mini_ATC.Up_handset();
            Abonent.Activation_button_of_tel_number(this, Mini_ATC.Last_number);
            Mini_ATC.Activate_all_button_of_MiniATC(this);
        }

        private void button_call_tel_1_Click(object sender, EventArgs e)
        {
            SoundPlayer play = new SoundPlayer();
            play.Stop();
            Chat Ch = new Chat();
            Ch.Owner = this;
            string number_of_calling_tel="";
            number_of_calling_tel = Convert.ToString(Mini_ATC.list_number[Mini_ATC.last_number-1].Number);
            Ch.label_tel.Text = Convert.ToString("Telephone #"+number_of_calling_tel);
            Ch.ShowDialog();
        }

        private void button_call_tel_2_Click(object sender, EventArgs e)
        {
            SoundPlayer play = new SoundPlayer();
            play.Stop();
            Chat Ch = new Chat();
            Ch.Owner = this;
            string number_of_calling_tel = "";
            number_of_calling_tel = Convert.ToString(Mini_ATC.list_number[Mini_ATC.last_number - 1].Number);
            Ch.label_tel.Text = Convert.ToString("Telephone #" + number_of_calling_tel);
            Ch.ShowDialog();
        }

        private void button_call_tel_3_Click(object sender, EventArgs e)
        {
            SoundPlayer play = new SoundPlayer();
            play.Stop();
            Chat Ch = new Chat();
            Ch.Owner = this;
            string number_of_calling_tel = "";
            number_of_calling_tel = Convert.ToString(Mini_ATC.list_number[Mini_ATC.last_number - 1].Number);
            Ch.label_tel.Text = Convert.ToString("Telephone #" + number_of_calling_tel);
            Ch.ShowDialog();
        }

        private void button_call_tel_4_Click(object sender, EventArgs e)
        {
            SoundPlayer play = new SoundPlayer();
            play.Stop();
            Chat Ch = new Chat();
            Ch.Owner = this;
            string number_of_calling_tel = "";
            number_of_calling_tel = Convert.ToString(Mini_ATC.list_number[Mini_ATC.last_number - 1].Number);
            Ch.label_tel.Text = Convert.ToString("Telephone #" + number_of_calling_tel);
            Ch.ShowDialog();
        }
    }
}
