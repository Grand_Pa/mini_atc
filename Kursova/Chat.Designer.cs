﻿namespace Kursova
{
    partial class Chat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_main = new System.Windows.Forms.Label();
            this.label_tel = new System.Windows.Forms.Label();
            this.textBox_chat_tel = new System.Windows.Forms.TextBox();
            this.textBox_chat_main = new System.Windows.Forms.TextBox();
            this.button_main_send = new System.Windows.Forms.Button();
            this.button_tel_send = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox_line_main = new System.Windows.Forms.TextBox();
            this.textBox_line_tel = new System.Windows.Forms.TextBox();
            this.button_tel_chat_down = new System.Windows.Forms.Button();
            this.button_main_chat_down = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label_main
            // 
            this.label_main.AutoSize = true;
            this.label_main.Location = new System.Drawing.Point(133, 9);
            this.label_main.Name = "label_main";
            this.label_main.Size = new System.Drawing.Size(30, 13);
            this.label_main.TabIndex = 1;
            this.label_main.Text = "Main";
            // 
            // label_tel
            // 
            this.label_tel.AutoSize = true;
            this.label_tel.Location = new System.Drawing.Point(400, 9);
            this.label_tel.Name = "label_tel";
            this.label_tel.Size = new System.Drawing.Size(0, 13);
            this.label_tel.TabIndex = 2;
            // 
            // textBox_chat_tel
            // 
            this.textBox_chat_tel.Location = new System.Drawing.Point(311, 25);
            this.textBox_chat_tel.Multiline = true;
            this.textBox_chat_tel.Name = "textBox_chat_tel";
            this.textBox_chat_tel.ReadOnly = true;
            this.textBox_chat_tel.Size = new System.Drawing.Size(260, 108);
            this.textBox_chat_tel.TabIndex = 3;
            // 
            // textBox_chat_main
            // 
            this.textBox_chat_main.Location = new System.Drawing.Point(12, 25);
            this.textBox_chat_main.Multiline = true;
            this.textBox_chat_main.Name = "textBox_chat_main";
            this.textBox_chat_main.ReadOnly = true;
            this.textBox_chat_main.Size = new System.Drawing.Size(282, 108);
            this.textBox_chat_main.TabIndex = 4;
            // 
            // button_main_send
            // 
            this.button_main_send.Location = new System.Drawing.Point(12, 170);
            this.button_main_send.Name = "button_main_send";
            this.button_main_send.Size = new System.Drawing.Size(75, 30);
            this.button_main_send.TabIndex = 5;
            this.button_main_send.Text = "Send";
            this.button_main_send.UseVisualStyleBackColor = true;
            this.button_main_send.Click += new System.EventHandler(this.button_main_send_Click);
            // 
            // button_tel_send
            // 
            this.button_tel_send.Location = new System.Drawing.Point(496, 168);
            this.button_tel_send.Name = "button_tel_send";
            this.button_tel_send.Size = new System.Drawing.Size(75, 32);
            this.button_tel_send.TabIndex = 6;
            this.button_tel_send.Text = "Send";
            this.button_tel_send.UseVisualStyleBackColor = true;
            this.button_tel_send.Click += new System.EventHandler(this.button_tel_send_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(300, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(5, 200);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // textBox_line_main
            // 
            this.textBox_line_main.Location = new System.Drawing.Point(12, 139);
            this.textBox_line_main.Name = "textBox_line_main";
            this.textBox_line_main.Size = new System.Drawing.Size(282, 20);
            this.textBox_line_main.TabIndex = 8;
            // 
            // textBox_line_tel
            // 
            this.textBox_line_tel.Location = new System.Drawing.Point(312, 139);
            this.textBox_line_tel.Name = "textBox_line_tel";
            this.textBox_line_tel.Size = new System.Drawing.Size(259, 20);
            this.textBox_line_tel.TabIndex = 9;
            // 
            // button_tel_chat_down
            // 
            this.button_tel_chat_down.Image = global::Kursova.Properties.Resources.red_trubka_27x75;
            this.button_tel_chat_down.Location = new System.Drawing.Point(312, 170);
            this.button_tel_chat_down.Name = "button_tel_chat_down";
            this.button_tel_chat_down.Size = new System.Drawing.Size(75, 30);
            this.button_tel_chat_down.TabIndex = 11;
            this.button_tel_chat_down.UseVisualStyleBackColor = true;
            this.button_tel_chat_down.Click += new System.EventHandler(this.button_tel_chat_down_Click);
            // 
            // button_main_chat_down
            // 
            this.button_main_chat_down.Image = global::Kursova.Properties.Resources.red_trubka_27x75;
            this.button_main_chat_down.Location = new System.Drawing.Point(219, 170);
            this.button_main_chat_down.Name = "button_main_chat_down";
            this.button_main_chat_down.Size = new System.Drawing.Size(75, 30);
            this.button_main_chat_down.TabIndex = 10;
            this.button_main_chat_down.UseVisualStyleBackColor = true;
            this.button_main_chat_down.Click += new System.EventHandler(this.button_main_chat_down_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(603, 25);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(190, 80);
            this.textBox1.TabIndex = 12;
            this.textBox1.Visible = false;
            // 
            // Chat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 385);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button_tel_chat_down);
            this.Controls.Add(this.button_main_chat_down);
            this.Controls.Add(this.textBox_line_tel);
            this.Controls.Add(this.textBox_line_main);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button_tel_send);
            this.Controls.Add(this.button_main_send);
            this.Controls.Add(this.textBox_chat_main);
            this.Controls.Add(this.textBox_chat_tel);
            this.Controls.Add(this.label_tel);
            this.Controls.Add(this.label_main);
            this.Name = "Chat";
            this.Text = "Chat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label_main;
        private System.Windows.Forms.TextBox textBox_chat_tel;
        private System.Windows.Forms.TextBox textBox_chat_main;
        private System.Windows.Forms.Button button_main_send;
        private System.Windows.Forms.Button button_tel_send;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Label label_tel;
        private System.Windows.Forms.TextBox textBox_line_main;
        private System.Windows.Forms.TextBox textBox_line_tel;
        private System.Windows.Forms.Button button_main_chat_down;
        private System.Windows.Forms.Button button_tel_chat_down;
        private System.Windows.Forms.TextBox textBox1;
    }
}