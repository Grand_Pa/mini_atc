﻿namespace Kursova
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button_call_tel_1 = new System.Windows.Forms.Button();
            this.button_central_tel_1 = new System.Windows.Forms.Button();
            this.button_down_tel_1 = new System.Windows.Forms.Button();
            this.textBox_tel_1 = new System.Windows.Forms.TextBox();
            this.button_12_tel_1 = new System.Windows.Forms.Button();
            this.button_1_tel_1 = new System.Windows.Forms.Button();
            this.button_0_tel_1 = new System.Windows.Forms.Button();
            this.button_11_tel_1 = new System.Windows.Forms.Button();
            this.button_9_tel_1 = new System.Windows.Forms.Button();
            this.button_8_tel_1 = new System.Windows.Forms.Button();
            this.button_7_tel_1 = new System.Windows.Forms.Button();
            this.button_6_tel_1 = new System.Windows.Forms.Button();
            this.button_5_tel_1 = new System.Windows.Forms.Button();
            this.button_4_tel_1 = new System.Windows.Forms.Button();
            this.button_3_tel_1 = new System.Windows.Forms.Button();
            this.button_2_tel_1 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button_call_tel_2 = new System.Windows.Forms.Button();
            this.textBox_tel_2 = new System.Windows.Forms.TextBox();
            this.button_12_tel_2 = new System.Windows.Forms.Button();
            this.button_0_tel_2 = new System.Windows.Forms.Button();
            this.button_11_tel_2 = new System.Windows.Forms.Button();
            this.button_9_tel_2 = new System.Windows.Forms.Button();
            this.button_8_tel_2 = new System.Windows.Forms.Button();
            this.button_7_tel_2 = new System.Windows.Forms.Button();
            this.button_6_tel_2 = new System.Windows.Forms.Button();
            this.button_5_tel_2 = new System.Windows.Forms.Button();
            this.button_4_tel_2 = new System.Windows.Forms.Button();
            this.button_3_tel_2 = new System.Windows.Forms.Button();
            this.button_2_tel_2 = new System.Windows.Forms.Button();
            this.button_1_tel_2 = new System.Windows.Forms.Button();
            this.button_central_tel_2 = new System.Windows.Forms.Button();
            this.button_down_tel_2 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button_main_backspace = new System.Windows.Forms.Button();
            this.button_main_12 = new System.Windows.Forms.Button();
            this.button_main_0 = new System.Windows.Forms.Button();
            this.button_main_11 = new System.Windows.Forms.Button();
            this.button_main_9 = new System.Windows.Forms.Button();
            this.button_main_8 = new System.Windows.Forms.Button();
            this.button_main_7 = new System.Windows.Forms.Button();
            this.button_main_6 = new System.Windows.Forms.Button();
            this.button_main_5 = new System.Windows.Forms.Button();
            this.button_main_4 = new System.Windows.Forms.Button();
            this.button_main_3 = new System.Windows.Forms.Button();
            this.button_main_2 = new System.Windows.Forms.Button();
            this.button_main_1 = new System.Windows.Forms.Button();
            this.textBox_main = new System.Windows.Forms.TextBox();
            this.button_main_call = new System.Windows.Forms.Button();
            this.button_main_cental = new System.Windows.Forms.Button();
            this.button_main_down = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button_call_tel_3 = new System.Windows.Forms.Button();
            this.button_central_tel_3 = new System.Windows.Forms.Button();
            this.button_down_tel_3 = new System.Windows.Forms.Button();
            this.button_12_tel_3 = new System.Windows.Forms.Button();
            this.button_0_tel_3 = new System.Windows.Forms.Button();
            this.button_11_tel_3 = new System.Windows.Forms.Button();
            this.button_9_tel_3 = new System.Windows.Forms.Button();
            this.button_8_tel_3 = new System.Windows.Forms.Button();
            this.button_7_tel_3 = new System.Windows.Forms.Button();
            this.button_6_tel_3 = new System.Windows.Forms.Button();
            this.button_5_tel_3 = new System.Windows.Forms.Button();
            this.button_4_tel_3 = new System.Windows.Forms.Button();
            this.button_3_tel_3 = new System.Windows.Forms.Button();
            this.button_2_tel_3 = new System.Windows.Forms.Button();
            this.button_1_tel_3 = new System.Windows.Forms.Button();
            this.textBox_tel_3 = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button_call_tel_4 = new System.Windows.Forms.Button();
            this.button_central_tel_4 = new System.Windows.Forms.Button();
            this.button_down_tel_4 = new System.Windows.Forms.Button();
            this.button_12_tel_4 = new System.Windows.Forms.Button();
            this.button_0_tel_4 = new System.Windows.Forms.Button();
            this.button_11_tel_4 = new System.Windows.Forms.Button();
            this.button_9_tel_4 = new System.Windows.Forms.Button();
            this.button_8_tel_4 = new System.Windows.Forms.Button();
            this.button_7_tel_4 = new System.Windows.Forms.Button();
            this.button_6_tel_4 = new System.Windows.Forms.Button();
            this.button_5_tel_4 = new System.Windows.Forms.Button();
            this.button_4_tel_4 = new System.Windows.Forms.Button();
            this.button_3_tel_4 = new System.Windows.Forms.Button();
            this.button_2_tel_4 = new System.Windows.Forms.Button();
            this.button_1_tel_4 = new System.Windows.Forms.Button();
            this.textBox_tel_4 = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(-4, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(323, 345);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage1.Controls.Add(this.button_call_tel_1);
            this.tabPage1.Controls.Add(this.button_central_tel_1);
            this.tabPage1.Controls.Add(this.button_down_tel_1);
            this.tabPage1.Controls.Add(this.textBox_tel_1);
            this.tabPage1.Controls.Add(this.button_12_tel_1);
            this.tabPage1.Controls.Add(this.button_1_tel_1);
            this.tabPage1.Controls.Add(this.button_0_tel_1);
            this.tabPage1.Controls.Add(this.button_11_tel_1);
            this.tabPage1.Controls.Add(this.button_9_tel_1);
            this.tabPage1.Controls.Add(this.button_8_tel_1);
            this.tabPage1.Controls.Add(this.button_7_tel_1);
            this.tabPage1.Controls.Add(this.button_6_tel_1);
            this.tabPage1.Controls.Add(this.button_5_tel_1);
            this.tabPage1.Controls.Add(this.button_4_tel_1);
            this.tabPage1.Controls.Add(this.button_3_tel_1);
            this.tabPage1.Controls.Add(this.button_2_tel_1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(315, 319);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Teleph.1";
            // 
            // button_call_tel_1
            // 
            this.button_call_tel_1.Enabled = false;
            this.button_call_tel_1.Image = global::Kursova.Properties.Resources._50x50;
            this.button_call_tel_1.Location = new System.Drawing.Point(230, 90);
            this.button_call_tel_1.Name = "button_call_tel_1";
            this.button_call_tel_1.Size = new System.Drawing.Size(57, 44);
            this.button_call_tel_1.TabIndex = 18;
            this.button_call_tel_1.UseVisualStyleBackColor = true;
            this.button_call_tel_1.Click += new System.EventHandler(this.button_call_tel_1_Click);
            // 
            // button_central_tel_1
            // 
            this.button_central_tel_1.Image = global::Kursova.Properties.Resources._50x50_cental_;
            this.button_central_tel_1.Location = new System.Drawing.Point(123, 90);
            this.button_central_tel_1.Name = "button_central_tel_1";
            this.button_central_tel_1.Size = new System.Drawing.Size(57, 44);
            this.button_central_tel_1.TabIndex = 17;
            this.button_central_tel_1.UseVisualStyleBackColor = true;
            this.button_central_tel_1.Click += new System.EventHandler(this.button_central_tel_1_Click);
            // 
            // button_down_tel_1
            // 
            this.button_down_tel_1.Enabled = false;
            this.button_down_tel_1.Image = global::Kursova.Properties.Resources._50x50_red_;
            this.button_down_tel_1.Location = new System.Drawing.Point(24, 90);
            this.button_down_tel_1.Name = "button_down_tel_1";
            this.button_down_tel_1.Size = new System.Drawing.Size(57, 44);
            this.button_down_tel_1.TabIndex = 16;
            this.button_down_tel_1.UseVisualStyleBackColor = true;
            this.button_down_tel_1.Click += new System.EventHandler(this.button_down_tel_1_Click);
            // 
            // textBox_tel_1
            // 
            this.textBox_tel_1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.textBox_tel_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_tel_1.Location = new System.Drawing.Point(39, 24);
            this.textBox_tel_1.Multiline = true;
            this.textBox_tel_1.Name = "textBox_tel_1";
            this.textBox_tel_1.ReadOnly = true;
            this.textBox_tel_1.Size = new System.Drawing.Size(235, 43);
            this.textBox_tel_1.TabIndex = 1;
            this.textBox_tel_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button_12_tel_1
            // 
            this.button_12_tel_1.Enabled = false;
            this.button_12_tel_1.Location = new System.Drawing.Point(221, 282);
            this.button_12_tel_1.Name = "button_12_tel_1";
            this.button_12_tel_1.Size = new System.Drawing.Size(75, 26);
            this.button_12_tel_1.TabIndex = 15;
            this.button_12_tel_1.Text = "#";
            this.button_12_tel_1.UseVisualStyleBackColor = true;
            // 
            // button_1_tel_1
            // 
            this.button_1_tel_1.Enabled = false;
            this.button_1_tel_1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_1_tel_1.Location = new System.Drawing.Point(11, 153);
            this.button_1_tel_1.Name = "button_1_tel_1";
            this.button_1_tel_1.Size = new System.Drawing.Size(75, 26);
            this.button_1_tel_1.TabIndex = 4;
            this.button_1_tel_1.Text = "1";
            this.button_1_tel_1.UseVisualStyleBackColor = true;
            // 
            // button_0_tel_1
            // 
            this.button_0_tel_1.Enabled = false;
            this.button_0_tel_1.Location = new System.Drawing.Point(114, 282);
            this.button_0_tel_1.Name = "button_0_tel_1";
            this.button_0_tel_1.Size = new System.Drawing.Size(75, 26);
            this.button_0_tel_1.TabIndex = 14;
            this.button_0_tel_1.Text = "0";
            this.button_0_tel_1.UseVisualStyleBackColor = true;
            // 
            // button_11_tel_1
            // 
            this.button_11_tel_1.Enabled = false;
            this.button_11_tel_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_11_tel_1.Location = new System.Drawing.Point(11, 282);
            this.button_11_tel_1.Name = "button_11_tel_1";
            this.button_11_tel_1.Size = new System.Drawing.Size(75, 26);
            this.button_11_tel_1.TabIndex = 13;
            this.button_11_tel_1.Text = "*";
            this.button_11_tel_1.UseVisualStyleBackColor = true;
            // 
            // button_9_tel_1
            // 
            this.button_9_tel_1.Enabled = false;
            this.button_9_tel_1.Location = new System.Drawing.Point(221, 240);
            this.button_9_tel_1.Name = "button_9_tel_1";
            this.button_9_tel_1.Size = new System.Drawing.Size(75, 26);
            this.button_9_tel_1.TabIndex = 12;
            this.button_9_tel_1.Text = "9";
            this.button_9_tel_1.UseVisualStyleBackColor = true;
            // 
            // button_8_tel_1
            // 
            this.button_8_tel_1.Enabled = false;
            this.button_8_tel_1.Location = new System.Drawing.Point(114, 240);
            this.button_8_tel_1.Name = "button_8_tel_1";
            this.button_8_tel_1.Size = new System.Drawing.Size(75, 26);
            this.button_8_tel_1.TabIndex = 11;
            this.button_8_tel_1.Text = "8";
            this.button_8_tel_1.UseVisualStyleBackColor = true;
            // 
            // button_7_tel_1
            // 
            this.button_7_tel_1.Enabled = false;
            this.button_7_tel_1.Location = new System.Drawing.Point(11, 240);
            this.button_7_tel_1.Name = "button_7_tel_1";
            this.button_7_tel_1.Size = new System.Drawing.Size(75, 26);
            this.button_7_tel_1.TabIndex = 10;
            this.button_7_tel_1.Text = "7";
            this.button_7_tel_1.UseVisualStyleBackColor = true;
            // 
            // button_6_tel_1
            // 
            this.button_6_tel_1.Enabled = false;
            this.button_6_tel_1.Location = new System.Drawing.Point(221, 194);
            this.button_6_tel_1.Name = "button_6_tel_1";
            this.button_6_tel_1.Size = new System.Drawing.Size(75, 26);
            this.button_6_tel_1.TabIndex = 9;
            this.button_6_tel_1.Text = "6";
            this.button_6_tel_1.UseVisualStyleBackColor = true;
            // 
            // button_5_tel_1
            // 
            this.button_5_tel_1.Enabled = false;
            this.button_5_tel_1.Location = new System.Drawing.Point(114, 194);
            this.button_5_tel_1.Name = "button_5_tel_1";
            this.button_5_tel_1.Size = new System.Drawing.Size(75, 26);
            this.button_5_tel_1.TabIndex = 8;
            this.button_5_tel_1.Text = "5";
            this.button_5_tel_1.UseVisualStyleBackColor = true;
            // 
            // button_4_tel_1
            // 
            this.button_4_tel_1.Enabled = false;
            this.button_4_tel_1.Location = new System.Drawing.Point(11, 194);
            this.button_4_tel_1.Name = "button_4_tel_1";
            this.button_4_tel_1.Size = new System.Drawing.Size(75, 26);
            this.button_4_tel_1.TabIndex = 7;
            this.button_4_tel_1.Text = "4";
            this.button_4_tel_1.UseVisualStyleBackColor = true;
            // 
            // button_3_tel_1
            // 
            this.button_3_tel_1.Enabled = false;
            this.button_3_tel_1.Location = new System.Drawing.Point(221, 153);
            this.button_3_tel_1.Name = "button_3_tel_1";
            this.button_3_tel_1.Size = new System.Drawing.Size(75, 26);
            this.button_3_tel_1.TabIndex = 6;
            this.button_3_tel_1.Text = "3";
            this.button_3_tel_1.UseVisualStyleBackColor = true;
            // 
            // button_2_tel_1
            // 
            this.button_2_tel_1.Enabled = false;
            this.button_2_tel_1.Location = new System.Drawing.Point(114, 153);
            this.button_2_tel_1.Name = "button_2_tel_1";
            this.button_2_tel_1.Size = new System.Drawing.Size(75, 26);
            this.button_2_tel_1.TabIndex = 5;
            this.button_2_tel_1.Text = "2";
            this.button_2_tel_1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage2.Controls.Add(this.button_call_tel_2);
            this.tabPage2.Controls.Add(this.textBox_tel_2);
            this.tabPage2.Controls.Add(this.button_12_tel_2);
            this.tabPage2.Controls.Add(this.button_0_tel_2);
            this.tabPage2.Controls.Add(this.button_11_tel_2);
            this.tabPage2.Controls.Add(this.button_9_tel_2);
            this.tabPage2.Controls.Add(this.button_8_tel_2);
            this.tabPage2.Controls.Add(this.button_7_tel_2);
            this.tabPage2.Controls.Add(this.button_6_tel_2);
            this.tabPage2.Controls.Add(this.button_5_tel_2);
            this.tabPage2.Controls.Add(this.button_4_tel_2);
            this.tabPage2.Controls.Add(this.button_3_tel_2);
            this.tabPage2.Controls.Add(this.button_2_tel_2);
            this.tabPage2.Controls.Add(this.button_1_tel_2);
            this.tabPage2.Controls.Add(this.button_central_tel_2);
            this.tabPage2.Controls.Add(this.button_down_tel_2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(315, 319);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Teleph. 2";
            // 
            // button_call_tel_2
            // 
            this.button_call_tel_2.Enabled = false;
            this.button_call_tel_2.Image = global::Kursova.Properties.Resources._50x50;
            this.button_call_tel_2.Location = new System.Drawing.Point(230, 90);
            this.button_call_tel_2.Name = "button_call_tel_2";
            this.button_call_tel_2.Size = new System.Drawing.Size(57, 44);
            this.button_call_tel_2.TabIndex = 15;
            this.button_call_tel_2.UseVisualStyleBackColor = true;
            this.button_call_tel_2.Click += new System.EventHandler(this.button_call_tel_2_Click);
            // 
            // textBox_tel_2
            // 
            this.textBox_tel_2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.textBox_tel_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_tel_2.Location = new System.Drawing.Point(39, 24);
            this.textBox_tel_2.Multiline = true;
            this.textBox_tel_2.Name = "textBox_tel_2";
            this.textBox_tel_2.ReadOnly = true;
            this.textBox_tel_2.Size = new System.Drawing.Size(235, 43);
            this.textBox_tel_2.TabIndex = 0;
            this.textBox_tel_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button_12_tel_2
            // 
            this.button_12_tel_2.Enabled = false;
            this.button_12_tel_2.Location = new System.Drawing.Point(221, 282);
            this.button_12_tel_2.Name = "button_12_tel_2";
            this.button_12_tel_2.Size = new System.Drawing.Size(75, 26);
            this.button_12_tel_2.TabIndex = 12;
            this.button_12_tel_2.Text = "#";
            this.button_12_tel_2.UseVisualStyleBackColor = true;
            // 
            // button_0_tel_2
            // 
            this.button_0_tel_2.Enabled = false;
            this.button_0_tel_2.Location = new System.Drawing.Point(114, 282);
            this.button_0_tel_2.Name = "button_0_tel_2";
            this.button_0_tel_2.Size = new System.Drawing.Size(75, 26);
            this.button_0_tel_2.TabIndex = 11;
            this.button_0_tel_2.Text = "0";
            this.button_0_tel_2.UseVisualStyleBackColor = true;
            // 
            // button_11_tel_2
            // 
            this.button_11_tel_2.Enabled = false;
            this.button_11_tel_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_11_tel_2.Location = new System.Drawing.Point(11, 282);
            this.button_11_tel_2.Name = "button_11_tel_2";
            this.button_11_tel_2.Size = new System.Drawing.Size(75, 26);
            this.button_11_tel_2.TabIndex = 10;
            this.button_11_tel_2.Text = "*";
            this.button_11_tel_2.UseVisualStyleBackColor = true;
            // 
            // button_9_tel_2
            // 
            this.button_9_tel_2.Enabled = false;
            this.button_9_tel_2.Location = new System.Drawing.Point(221, 240);
            this.button_9_tel_2.Name = "button_9_tel_2";
            this.button_9_tel_2.Size = new System.Drawing.Size(75, 26);
            this.button_9_tel_2.TabIndex = 9;
            this.button_9_tel_2.Text = "9";
            this.button_9_tel_2.UseVisualStyleBackColor = true;
            // 
            // button_8_tel_2
            // 
            this.button_8_tel_2.Enabled = false;
            this.button_8_tel_2.Location = new System.Drawing.Point(114, 240);
            this.button_8_tel_2.Name = "button_8_tel_2";
            this.button_8_tel_2.Size = new System.Drawing.Size(75, 26);
            this.button_8_tel_2.TabIndex = 8;
            this.button_8_tel_2.Text = "8";
            this.button_8_tel_2.UseVisualStyleBackColor = true;
            // 
            // button_7_tel_2
            // 
            this.button_7_tel_2.Enabled = false;
            this.button_7_tel_2.Location = new System.Drawing.Point(11, 240);
            this.button_7_tel_2.Name = "button_7_tel_2";
            this.button_7_tel_2.Size = new System.Drawing.Size(75, 26);
            this.button_7_tel_2.TabIndex = 7;
            this.button_7_tel_2.Text = "7";
            this.button_7_tel_2.UseVisualStyleBackColor = true;
            // 
            // button_6_tel_2
            // 
            this.button_6_tel_2.Enabled = false;
            this.button_6_tel_2.Location = new System.Drawing.Point(221, 194);
            this.button_6_tel_2.Name = "button_6_tel_2";
            this.button_6_tel_2.Size = new System.Drawing.Size(75, 26);
            this.button_6_tel_2.TabIndex = 6;
            this.button_6_tel_2.Text = "6";
            this.button_6_tel_2.UseVisualStyleBackColor = true;
            // 
            // button_5_tel_2
            // 
            this.button_5_tel_2.Enabled = false;
            this.button_5_tel_2.Location = new System.Drawing.Point(114, 194);
            this.button_5_tel_2.Name = "button_5_tel_2";
            this.button_5_tel_2.Size = new System.Drawing.Size(75, 26);
            this.button_5_tel_2.TabIndex = 5;
            this.button_5_tel_2.Text = "5";
            this.button_5_tel_2.UseVisualStyleBackColor = true;
            // 
            // button_4_tel_2
            // 
            this.button_4_tel_2.Enabled = false;
            this.button_4_tel_2.Location = new System.Drawing.Point(11, 194);
            this.button_4_tel_2.Name = "button_4_tel_2";
            this.button_4_tel_2.Size = new System.Drawing.Size(75, 26);
            this.button_4_tel_2.TabIndex = 4;
            this.button_4_tel_2.Text = "4";
            this.button_4_tel_2.UseVisualStyleBackColor = true;
            // 
            // button_3_tel_2
            // 
            this.button_3_tel_2.Enabled = false;
            this.button_3_tel_2.Location = new System.Drawing.Point(221, 153);
            this.button_3_tel_2.Name = "button_3_tel_2";
            this.button_3_tel_2.Size = new System.Drawing.Size(75, 26);
            this.button_3_tel_2.TabIndex = 3;
            this.button_3_tel_2.Text = "3";
            this.button_3_tel_2.UseVisualStyleBackColor = true;
            // 
            // button_2_tel_2
            // 
            this.button_2_tel_2.Enabled = false;
            this.button_2_tel_2.Location = new System.Drawing.Point(114, 153);
            this.button_2_tel_2.Name = "button_2_tel_2";
            this.button_2_tel_2.Size = new System.Drawing.Size(75, 26);
            this.button_2_tel_2.TabIndex = 2;
            this.button_2_tel_2.Text = "2";
            this.button_2_tel_2.UseVisualStyleBackColor = true;
            // 
            // button_1_tel_2
            // 
            this.button_1_tel_2.Enabled = false;
            this.button_1_tel_2.Location = new System.Drawing.Point(11, 153);
            this.button_1_tel_2.Name = "button_1_tel_2";
            this.button_1_tel_2.Size = new System.Drawing.Size(75, 26);
            this.button_1_tel_2.TabIndex = 1;
            this.button_1_tel_2.Text = "1";
            this.button_1_tel_2.UseVisualStyleBackColor = true;
            // 
            // button_central_tel_2
            // 
            this.button_central_tel_2.Image = global::Kursova.Properties.Resources._50x50_cental_;
            this.button_central_tel_2.Location = new System.Drawing.Point(123, 90);
            this.button_central_tel_2.Name = "button_central_tel_2";
            this.button_central_tel_2.Size = new System.Drawing.Size(57, 44);
            this.button_central_tel_2.TabIndex = 14;
            this.button_central_tel_2.UseVisualStyleBackColor = true;
            this.button_central_tel_2.Click += new System.EventHandler(this.button_central_tel_2_Click);
            // 
            // button_down_tel_2
            // 
            this.button_down_tel_2.Enabled = false;
            this.button_down_tel_2.Image = global::Kursova.Properties.Resources._50x50_red_;
            this.button_down_tel_2.Location = new System.Drawing.Point(24, 90);
            this.button_down_tel_2.Name = "button_down_tel_2";
            this.button_down_tel_2.Size = new System.Drawing.Size(57, 44);
            this.button_down_tel_2.TabIndex = 13;
            this.button_down_tel_2.UseVisualStyleBackColor = true;
            this.button_down_tel_2.Click += new System.EventHandler(this.button_down_tel_2_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage3.Controls.Add(this.button_main_backspace);
            this.tabPage3.Controls.Add(this.button_main_12);
            this.tabPage3.Controls.Add(this.button_main_0);
            this.tabPage3.Controls.Add(this.button_main_11);
            this.tabPage3.Controls.Add(this.button_main_9);
            this.tabPage3.Controls.Add(this.button_main_8);
            this.tabPage3.Controls.Add(this.button_main_7);
            this.tabPage3.Controls.Add(this.button_main_6);
            this.tabPage3.Controls.Add(this.button_main_5);
            this.tabPage3.Controls.Add(this.button_main_4);
            this.tabPage3.Controls.Add(this.button_main_3);
            this.tabPage3.Controls.Add(this.button_main_2);
            this.tabPage3.Controls.Add(this.button_main_1);
            this.tabPage3.Controls.Add(this.textBox_main);
            this.tabPage3.Controls.Add(this.button_main_call);
            this.tabPage3.Controls.Add(this.button_main_cental);
            this.tabPage3.Controls.Add(this.button_main_down);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(315, 319);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Main T.";
            // 
            // button_main_backspace
            // 
            this.button_main_backspace.Enabled = false;
            this.button_main_backspace.Image = global::Kursova.Properties.Resources._25x25_Backspace2_;
            this.button_main_backspace.Location = new System.Drawing.Point(3, 35);
            this.button_main_backspace.Name = "button_main_backspace";
            this.button_main_backspace.Size = new System.Drawing.Size(32, 23);
            this.button_main_backspace.TabIndex = 29;
            this.button_main_backspace.UseVisualStyleBackColor = true;
            this.button_main_backspace.Click += new System.EventHandler(this.button_main_backspace_Click);
            // 
            // button_main_12
            // 
            this.button_main_12.Enabled = false;
            this.button_main_12.Location = new System.Drawing.Point(221, 282);
            this.button_main_12.Name = "button_main_12";
            this.button_main_12.Size = new System.Drawing.Size(75, 26);
            this.button_main_12.TabIndex = 28;
            this.button_main_12.Text = "#";
            this.button_main_12.UseVisualStyleBackColor = true;
            this.button_main_12.Click += new System.EventHandler(this.button_main_12_Click);
            // 
            // button_main_0
            // 
            this.button_main_0.Enabled = false;
            this.button_main_0.Location = new System.Drawing.Point(114, 282);
            this.button_main_0.Name = "button_main_0";
            this.button_main_0.Size = new System.Drawing.Size(75, 26);
            this.button_main_0.TabIndex = 27;
            this.button_main_0.Text = "0";
            this.button_main_0.UseVisualStyleBackColor = true;
            this.button_main_0.Click += new System.EventHandler(this.button_main_0_Click);
            // 
            // button_main_11
            // 
            this.button_main_11.Enabled = false;
            this.button_main_11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_main_11.Location = new System.Drawing.Point(11, 282);
            this.button_main_11.Name = "button_main_11";
            this.button_main_11.Size = new System.Drawing.Size(75, 26);
            this.button_main_11.TabIndex = 26;
            this.button_main_11.Text = "*";
            this.button_main_11.UseVisualStyleBackColor = true;
            this.button_main_11.Click += new System.EventHandler(this.button_main_11_Click);
            // 
            // button_main_9
            // 
            this.button_main_9.Enabled = false;
            this.button_main_9.Location = new System.Drawing.Point(221, 240);
            this.button_main_9.Name = "button_main_9";
            this.button_main_9.Size = new System.Drawing.Size(75, 26);
            this.button_main_9.TabIndex = 25;
            this.button_main_9.Text = "9";
            this.button_main_9.UseVisualStyleBackColor = true;
            this.button_main_9.Click += new System.EventHandler(this.button_main_9_Click);
            // 
            // button_main_8
            // 
            this.button_main_8.Enabled = false;
            this.button_main_8.Location = new System.Drawing.Point(114, 240);
            this.button_main_8.Name = "button_main_8";
            this.button_main_8.Size = new System.Drawing.Size(75, 26);
            this.button_main_8.TabIndex = 24;
            this.button_main_8.Text = "8";
            this.button_main_8.UseVisualStyleBackColor = true;
            this.button_main_8.Click += new System.EventHandler(this.button_main_8_Click);
            // 
            // button_main_7
            // 
            this.button_main_7.Enabled = false;
            this.button_main_7.Location = new System.Drawing.Point(11, 240);
            this.button_main_7.Name = "button_main_7";
            this.button_main_7.Size = new System.Drawing.Size(75, 26);
            this.button_main_7.TabIndex = 23;
            this.button_main_7.Text = "7";
            this.button_main_7.UseVisualStyleBackColor = true;
            this.button_main_7.Click += new System.EventHandler(this.button_main_7_Click);
            // 
            // button_main_6
            // 
            this.button_main_6.Enabled = false;
            this.button_main_6.Location = new System.Drawing.Point(221, 194);
            this.button_main_6.Name = "button_main_6";
            this.button_main_6.Size = new System.Drawing.Size(75, 26);
            this.button_main_6.TabIndex = 22;
            this.button_main_6.Text = "6";
            this.button_main_6.UseVisualStyleBackColor = true;
            this.button_main_6.Click += new System.EventHandler(this.button_main_6_Click);
            // 
            // button_main_5
            // 
            this.button_main_5.Enabled = false;
            this.button_main_5.Location = new System.Drawing.Point(114, 194);
            this.button_main_5.Name = "button_main_5";
            this.button_main_5.Size = new System.Drawing.Size(75, 26);
            this.button_main_5.TabIndex = 21;
            this.button_main_5.Text = "5";
            this.button_main_5.UseVisualStyleBackColor = true;
            this.button_main_5.Click += new System.EventHandler(this.button_main_5_Click);
            // 
            // button_main_4
            // 
            this.button_main_4.Enabled = false;
            this.button_main_4.Location = new System.Drawing.Point(11, 194);
            this.button_main_4.Name = "button_main_4";
            this.button_main_4.Size = new System.Drawing.Size(75, 26);
            this.button_main_4.TabIndex = 20;
            this.button_main_4.Text = "4";
            this.button_main_4.UseVisualStyleBackColor = true;
            this.button_main_4.Click += new System.EventHandler(this.button_main_4_Click);
            // 
            // button_main_3
            // 
            this.button_main_3.Enabled = false;
            this.button_main_3.Location = new System.Drawing.Point(221, 153);
            this.button_main_3.Name = "button_main_3";
            this.button_main_3.Size = new System.Drawing.Size(75, 26);
            this.button_main_3.TabIndex = 19;
            this.button_main_3.Text = "3";
            this.button_main_3.UseVisualStyleBackColor = true;
            this.button_main_3.Click += new System.EventHandler(this.button_main_3_Click);
            // 
            // button_main_2
            // 
            this.button_main_2.Enabled = false;
            this.button_main_2.Location = new System.Drawing.Point(114, 153);
            this.button_main_2.Name = "button_main_2";
            this.button_main_2.Size = new System.Drawing.Size(75, 26);
            this.button_main_2.TabIndex = 18;
            this.button_main_2.Text = "2";
            this.button_main_2.UseVisualStyleBackColor = true;
            this.button_main_2.Click += new System.EventHandler(this.button_main_2_Click);
            // 
            // button_main_1
            // 
            this.button_main_1.Enabled = false;
            this.button_main_1.Location = new System.Drawing.Point(11, 153);
            this.button_main_1.Name = "button_main_1";
            this.button_main_1.Size = new System.Drawing.Size(75, 26);
            this.button_main_1.TabIndex = 17;
            this.button_main_1.Text = "1";
            this.button_main_1.UseVisualStyleBackColor = true;
            this.button_main_1.Click += new System.EventHandler(this.button_main_1_Click);
            // 
            // textBox_main
            // 
            this.textBox_main.BackColor = System.Drawing.SystemColors.ControlLight;
            this.textBox_main.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_main.Location = new System.Drawing.Point(39, 24);
            this.textBox_main.Multiline = true;
            this.textBox_main.Name = "textBox_main";
            this.textBox_main.ReadOnly = true;
            this.textBox_main.Size = new System.Drawing.Size(235, 43);
            this.textBox_main.TabIndex = 12;
            this.textBox_main.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button_main_call
            // 
            this.button_main_call.Enabled = false;
            this.button_main_call.Image = global::Kursova.Properties.Resources._50x50;
            this.button_main_call.Location = new System.Drawing.Point(230, 90);
            this.button_main_call.Name = "button_main_call";
            this.button_main_call.Size = new System.Drawing.Size(57, 44);
            this.button_main_call.TabIndex = 15;
            this.button_main_call.UseVisualStyleBackColor = true;
            this.button_main_call.Click += new System.EventHandler(this.button_main_call_Click);
            // 
            // button_main_cental
            // 
            this.button_main_cental.Image = global::Kursova.Properties.Resources._50x50_cental_;
            this.button_main_cental.Location = new System.Drawing.Point(123, 90);
            this.button_main_cental.Name = "button_main_cental";
            this.button_main_cental.Size = new System.Drawing.Size(57, 44);
            this.button_main_cental.TabIndex = 14;
            this.button_main_cental.UseVisualStyleBackColor = true;
            this.button_main_cental.Click += new System.EventHandler(this.button_main_cental_Click);
            // 
            // button_main_down
            // 
            this.button_main_down.Image = global::Kursova.Properties.Resources._50x50_red_;
            this.button_main_down.Location = new System.Drawing.Point(24, 90);
            this.button_main_down.Name = "button_main_down";
            this.button_main_down.Size = new System.Drawing.Size(57, 44);
            this.button_main_down.TabIndex = 13;
            this.button_main_down.UseVisualStyleBackColor = true;
            this.button_main_down.Click += new System.EventHandler(this.button_main_down_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage4.Controls.Add(this.button_call_tel_3);
            this.tabPage4.Controls.Add(this.button_central_tel_3);
            this.tabPage4.Controls.Add(this.button_down_tel_3);
            this.tabPage4.Controls.Add(this.button_12_tel_3);
            this.tabPage4.Controls.Add(this.button_0_tel_3);
            this.tabPage4.Controls.Add(this.button_11_tel_3);
            this.tabPage4.Controls.Add(this.button_9_tel_3);
            this.tabPage4.Controls.Add(this.button_8_tel_3);
            this.tabPage4.Controls.Add(this.button_7_tel_3);
            this.tabPage4.Controls.Add(this.button_6_tel_3);
            this.tabPage4.Controls.Add(this.button_5_tel_3);
            this.tabPage4.Controls.Add(this.button_4_tel_3);
            this.tabPage4.Controls.Add(this.button_3_tel_3);
            this.tabPage4.Controls.Add(this.button_2_tel_3);
            this.tabPage4.Controls.Add(this.button_1_tel_3);
            this.tabPage4.Controls.Add(this.textBox_tel_3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(315, 319);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Teleph. 3";
            // 
            // button_call_tel_3
            // 
            this.button_call_tel_3.Enabled = false;
            this.button_call_tel_3.Image = global::Kursova.Properties.Resources._50x50;
            this.button_call_tel_3.Location = new System.Drawing.Point(230, 90);
            this.button_call_tel_3.Name = "button_call_tel_3";
            this.button_call_tel_3.Size = new System.Drawing.Size(57, 44);
            this.button_call_tel_3.TabIndex = 16;
            this.button_call_tel_3.UseVisualStyleBackColor = true;
            this.button_call_tel_3.Click += new System.EventHandler(this.button_call_tel_3_Click);
            // 
            // button_central_tel_3
            // 
            this.button_central_tel_3.Image = global::Kursova.Properties.Resources._50x50_cental_;
            this.button_central_tel_3.Location = new System.Drawing.Point(123, 90);
            this.button_central_tel_3.Name = "button_central_tel_3";
            this.button_central_tel_3.Size = new System.Drawing.Size(57, 44);
            this.button_central_tel_3.TabIndex = 15;
            this.button_central_tel_3.UseVisualStyleBackColor = true;
            this.button_central_tel_3.Click += new System.EventHandler(this.button_central_tel_3_Click);
            // 
            // button_down_tel_3
            // 
            this.button_down_tel_3.Enabled = false;
            this.button_down_tel_3.Image = global::Kursova.Properties.Resources._50x50_red_;
            this.button_down_tel_3.Location = new System.Drawing.Point(24, 90);
            this.button_down_tel_3.Name = "button_down_tel_3";
            this.button_down_tel_3.Size = new System.Drawing.Size(57, 44);
            this.button_down_tel_3.TabIndex = 14;
            this.button_down_tel_3.UseVisualStyleBackColor = true;
            this.button_down_tel_3.Click += new System.EventHandler(this.button_down_tel_3_Click);
            // 
            // button_12_tel_3
            // 
            this.button_12_tel_3.Enabled = false;
            this.button_12_tel_3.Location = new System.Drawing.Point(221, 282);
            this.button_12_tel_3.Name = "button_12_tel_3";
            this.button_12_tel_3.Size = new System.Drawing.Size(75, 26);
            this.button_12_tel_3.TabIndex = 13;
            this.button_12_tel_3.Text = "#";
            this.button_12_tel_3.UseVisualStyleBackColor = true;
            // 
            // button_0_tel_3
            // 
            this.button_0_tel_3.Enabled = false;
            this.button_0_tel_3.Location = new System.Drawing.Point(114, 282);
            this.button_0_tel_3.Name = "button_0_tel_3";
            this.button_0_tel_3.Size = new System.Drawing.Size(75, 26);
            this.button_0_tel_3.TabIndex = 12;
            this.button_0_tel_3.Text = "0";
            this.button_0_tel_3.UseVisualStyleBackColor = true;
            // 
            // button_11_tel_3
            // 
            this.button_11_tel_3.Enabled = false;
            this.button_11_tel_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_11_tel_3.Location = new System.Drawing.Point(11, 282);
            this.button_11_tel_3.Name = "button_11_tel_3";
            this.button_11_tel_3.Size = new System.Drawing.Size(75, 26);
            this.button_11_tel_3.TabIndex = 11;
            this.button_11_tel_3.Text = "*";
            this.button_11_tel_3.UseVisualStyleBackColor = true;
            // 
            // button_9_tel_3
            // 
            this.button_9_tel_3.Enabled = false;
            this.button_9_tel_3.Location = new System.Drawing.Point(221, 240);
            this.button_9_tel_3.Name = "button_9_tel_3";
            this.button_9_tel_3.Size = new System.Drawing.Size(75, 26);
            this.button_9_tel_3.TabIndex = 10;
            this.button_9_tel_3.Text = "9";
            this.button_9_tel_3.UseVisualStyleBackColor = true;
            // 
            // button_8_tel_3
            // 
            this.button_8_tel_3.Enabled = false;
            this.button_8_tel_3.Location = new System.Drawing.Point(114, 240);
            this.button_8_tel_3.Name = "button_8_tel_3";
            this.button_8_tel_3.Size = new System.Drawing.Size(75, 26);
            this.button_8_tel_3.TabIndex = 9;
            this.button_8_tel_3.Text = "8";
            this.button_8_tel_3.UseVisualStyleBackColor = true;
            // 
            // button_7_tel_3
            // 
            this.button_7_tel_3.Enabled = false;
            this.button_7_tel_3.Location = new System.Drawing.Point(11, 240);
            this.button_7_tel_3.Name = "button_7_tel_3";
            this.button_7_tel_3.Size = new System.Drawing.Size(75, 26);
            this.button_7_tel_3.TabIndex = 8;
            this.button_7_tel_3.Text = "7";
            this.button_7_tel_3.UseVisualStyleBackColor = true;
            // 
            // button_6_tel_3
            // 
            this.button_6_tel_3.Enabled = false;
            this.button_6_tel_3.Location = new System.Drawing.Point(221, 194);
            this.button_6_tel_3.Name = "button_6_tel_3";
            this.button_6_tel_3.Size = new System.Drawing.Size(75, 26);
            this.button_6_tel_3.TabIndex = 7;
            this.button_6_tel_3.Text = "6";
            this.button_6_tel_3.UseVisualStyleBackColor = true;
            // 
            // button_5_tel_3
            // 
            this.button_5_tel_3.Enabled = false;
            this.button_5_tel_3.Location = new System.Drawing.Point(114, 194);
            this.button_5_tel_3.Name = "button_5_tel_3";
            this.button_5_tel_3.Size = new System.Drawing.Size(75, 26);
            this.button_5_tel_3.TabIndex = 6;
            this.button_5_tel_3.Text = "5";
            this.button_5_tel_3.UseVisualStyleBackColor = true;
            // 
            // button_4_tel_3
            // 
            this.button_4_tel_3.Enabled = false;
            this.button_4_tel_3.Location = new System.Drawing.Point(11, 194);
            this.button_4_tel_3.Name = "button_4_tel_3";
            this.button_4_tel_3.Size = new System.Drawing.Size(75, 26);
            this.button_4_tel_3.TabIndex = 5;
            this.button_4_tel_3.Text = "4";
            this.button_4_tel_3.UseVisualStyleBackColor = true;
            // 
            // button_3_tel_3
            // 
            this.button_3_tel_3.Enabled = false;
            this.button_3_tel_3.Location = new System.Drawing.Point(221, 153);
            this.button_3_tel_3.Name = "button_3_tel_3";
            this.button_3_tel_3.Size = new System.Drawing.Size(75, 26);
            this.button_3_tel_3.TabIndex = 4;
            this.button_3_tel_3.Text = "3";
            this.button_3_tel_3.UseVisualStyleBackColor = true;
            // 
            // button_2_tel_3
            // 
            this.button_2_tel_3.Enabled = false;
            this.button_2_tel_3.Location = new System.Drawing.Point(114, 153);
            this.button_2_tel_3.Name = "button_2_tel_3";
            this.button_2_tel_3.Size = new System.Drawing.Size(75, 26);
            this.button_2_tel_3.TabIndex = 3;
            this.button_2_tel_3.Text = "2";
            this.button_2_tel_3.UseVisualStyleBackColor = true;
            // 
            // button_1_tel_3
            // 
            this.button_1_tel_3.Enabled = false;
            this.button_1_tel_3.Location = new System.Drawing.Point(11, 153);
            this.button_1_tel_3.Name = "button_1_tel_3";
            this.button_1_tel_3.Size = new System.Drawing.Size(75, 26);
            this.button_1_tel_3.TabIndex = 2;
            this.button_1_tel_3.Text = "1";
            this.button_1_tel_3.UseVisualStyleBackColor = true;
            // 
            // textBox_tel_3
            // 
            this.textBox_tel_3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.textBox_tel_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_tel_3.Location = new System.Drawing.Point(39, 24);
            this.textBox_tel_3.Multiline = true;
            this.textBox_tel_3.Name = "textBox_tel_3";
            this.textBox_tel_3.ReadOnly = true;
            this.textBox_tel_3.Size = new System.Drawing.Size(235, 43);
            this.textBox_tel_3.TabIndex = 1;
            this.textBox_tel_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage5.Controls.Add(this.button_call_tel_4);
            this.tabPage5.Controls.Add(this.button_central_tel_4);
            this.tabPage5.Controls.Add(this.button_down_tel_4);
            this.tabPage5.Controls.Add(this.button_12_tel_4);
            this.tabPage5.Controls.Add(this.button_0_tel_4);
            this.tabPage5.Controls.Add(this.button_11_tel_4);
            this.tabPage5.Controls.Add(this.button_9_tel_4);
            this.tabPage5.Controls.Add(this.button_8_tel_4);
            this.tabPage5.Controls.Add(this.button_7_tel_4);
            this.tabPage5.Controls.Add(this.button_6_tel_4);
            this.tabPage5.Controls.Add(this.button_5_tel_4);
            this.tabPage5.Controls.Add(this.button_4_tel_4);
            this.tabPage5.Controls.Add(this.button_3_tel_4);
            this.tabPage5.Controls.Add(this.button_2_tel_4);
            this.tabPage5.Controls.Add(this.button_1_tel_4);
            this.tabPage5.Controls.Add(this.textBox_tel_4);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(315, 319);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Teleph. 4";
            // 
            // button_call_tel_4
            // 
            this.button_call_tel_4.Enabled = false;
            this.button_call_tel_4.Image = global::Kursova.Properties.Resources._50x50;
            this.button_call_tel_4.Location = new System.Drawing.Point(230, 90);
            this.button_call_tel_4.Name = "button_call_tel_4";
            this.button_call_tel_4.Size = new System.Drawing.Size(57, 44);
            this.button_call_tel_4.TabIndex = 16;
            this.button_call_tel_4.UseVisualStyleBackColor = true;
            this.button_call_tel_4.Click += new System.EventHandler(this.button_call_tel_4_Click);
            // 
            // button_central_tel_4
            // 
            this.button_central_tel_4.Image = global::Kursova.Properties.Resources._50x50_cental_;
            this.button_central_tel_4.Location = new System.Drawing.Point(123, 90);
            this.button_central_tel_4.Name = "button_central_tel_4";
            this.button_central_tel_4.Size = new System.Drawing.Size(57, 44);
            this.button_central_tel_4.TabIndex = 15;
            this.button_central_tel_4.UseVisualStyleBackColor = true;
            this.button_central_tel_4.Click += new System.EventHandler(this.button_central_tel_4_Click);
            // 
            // button_down_tel_4
            // 
            this.button_down_tel_4.Enabled = false;
            this.button_down_tel_4.Image = global::Kursova.Properties.Resources._50x50_red_;
            this.button_down_tel_4.Location = new System.Drawing.Point(24, 90);
            this.button_down_tel_4.Name = "button_down_tel_4";
            this.button_down_tel_4.Size = new System.Drawing.Size(57, 44);
            this.button_down_tel_4.TabIndex = 14;
            this.button_down_tel_4.UseVisualStyleBackColor = true;
            this.button_down_tel_4.Click += new System.EventHandler(this.button_down_tel_4_Click);
            // 
            // button_12_tel_4
            // 
            this.button_12_tel_4.Enabled = false;
            this.button_12_tel_4.Location = new System.Drawing.Point(221, 282);
            this.button_12_tel_4.Name = "button_12_tel_4";
            this.button_12_tel_4.Size = new System.Drawing.Size(75, 26);
            this.button_12_tel_4.TabIndex = 13;
            this.button_12_tel_4.Text = "#";
            this.button_12_tel_4.UseVisualStyleBackColor = true;
            // 
            // button_0_tel_4
            // 
            this.button_0_tel_4.Enabled = false;
            this.button_0_tel_4.Location = new System.Drawing.Point(114, 282);
            this.button_0_tel_4.Name = "button_0_tel_4";
            this.button_0_tel_4.Size = new System.Drawing.Size(75, 26);
            this.button_0_tel_4.TabIndex = 12;
            this.button_0_tel_4.Text = "0";
            this.button_0_tel_4.UseVisualStyleBackColor = true;
            // 
            // button_11_tel_4
            // 
            this.button_11_tel_4.Enabled = false;
            this.button_11_tel_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_11_tel_4.Location = new System.Drawing.Point(11, 282);
            this.button_11_tel_4.Name = "button_11_tel_4";
            this.button_11_tel_4.Size = new System.Drawing.Size(75, 26);
            this.button_11_tel_4.TabIndex = 11;
            this.button_11_tel_4.Text = "*";
            this.button_11_tel_4.UseVisualStyleBackColor = true;
            // 
            // button_9_tel_4
            // 
            this.button_9_tel_4.Enabled = false;
            this.button_9_tel_4.Location = new System.Drawing.Point(221, 240);
            this.button_9_tel_4.Name = "button_9_tel_4";
            this.button_9_tel_4.Size = new System.Drawing.Size(75, 26);
            this.button_9_tel_4.TabIndex = 10;
            this.button_9_tel_4.Text = "9";
            this.button_9_tel_4.UseVisualStyleBackColor = true;
            // 
            // button_8_tel_4
            // 
            this.button_8_tel_4.Enabled = false;
            this.button_8_tel_4.Location = new System.Drawing.Point(114, 240);
            this.button_8_tel_4.Name = "button_8_tel_4";
            this.button_8_tel_4.Size = new System.Drawing.Size(75, 26);
            this.button_8_tel_4.TabIndex = 9;
            this.button_8_tel_4.Text = "8";
            this.button_8_tel_4.UseVisualStyleBackColor = true;
            // 
            // button_7_tel_4
            // 
            this.button_7_tel_4.Enabled = false;
            this.button_7_tel_4.Location = new System.Drawing.Point(11, 240);
            this.button_7_tel_4.Name = "button_7_tel_4";
            this.button_7_tel_4.Size = new System.Drawing.Size(75, 26);
            this.button_7_tel_4.TabIndex = 8;
            this.button_7_tel_4.Text = "7";
            this.button_7_tel_4.UseVisualStyleBackColor = true;
            // 
            // button_6_tel_4
            // 
            this.button_6_tel_4.Enabled = false;
            this.button_6_tel_4.Location = new System.Drawing.Point(221, 194);
            this.button_6_tel_4.Name = "button_6_tel_4";
            this.button_6_tel_4.Size = new System.Drawing.Size(75, 26);
            this.button_6_tel_4.TabIndex = 7;
            this.button_6_tel_4.Text = "6";
            this.button_6_tel_4.UseVisualStyleBackColor = true;
            // 
            // button_5_tel_4
            // 
            this.button_5_tel_4.Enabled = false;
            this.button_5_tel_4.Location = new System.Drawing.Point(114, 194);
            this.button_5_tel_4.Name = "button_5_tel_4";
            this.button_5_tel_4.Size = new System.Drawing.Size(75, 26);
            this.button_5_tel_4.TabIndex = 6;
            this.button_5_tel_4.Text = "5";
            this.button_5_tel_4.UseVisualStyleBackColor = true;
            // 
            // button_4_tel_4
            // 
            this.button_4_tel_4.Enabled = false;
            this.button_4_tel_4.Location = new System.Drawing.Point(11, 194);
            this.button_4_tel_4.Name = "button_4_tel_4";
            this.button_4_tel_4.Size = new System.Drawing.Size(75, 26);
            this.button_4_tel_4.TabIndex = 5;
            this.button_4_tel_4.Text = "4";
            this.button_4_tel_4.UseVisualStyleBackColor = true;
            // 
            // button_3_tel_4
            // 
            this.button_3_tel_4.Enabled = false;
            this.button_3_tel_4.Location = new System.Drawing.Point(221, 153);
            this.button_3_tel_4.Name = "button_3_tel_4";
            this.button_3_tel_4.Size = new System.Drawing.Size(75, 26);
            this.button_3_tel_4.TabIndex = 4;
            this.button_3_tel_4.Text = "3";
            this.button_3_tel_4.UseVisualStyleBackColor = true;
            // 
            // button_2_tel_4
            // 
            this.button_2_tel_4.Enabled = false;
            this.button_2_tel_4.Location = new System.Drawing.Point(114, 153);
            this.button_2_tel_4.Name = "button_2_tel_4";
            this.button_2_tel_4.Size = new System.Drawing.Size(75, 26);
            this.button_2_tel_4.TabIndex = 3;
            this.button_2_tel_4.Text = "2";
            this.button_2_tel_4.UseVisualStyleBackColor = true;
            // 
            // button_1_tel_4
            // 
            this.button_1_tel_4.Enabled = false;
            this.button_1_tel_4.Location = new System.Drawing.Point(11, 153);
            this.button_1_tel_4.Name = "button_1_tel_4";
            this.button_1_tel_4.Size = new System.Drawing.Size(75, 26);
            this.button_1_tel_4.TabIndex = 2;
            this.button_1_tel_4.Text = "1";
            this.button_1_tel_4.UseVisualStyleBackColor = true;
            // 
            // textBox_tel_4
            // 
            this.textBox_tel_4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.textBox_tel_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_tel_4.Location = new System.Drawing.Point(39, 24);
            this.textBox_tel_4.Multiline = true;
            this.textBox_tel_4.Name = "textBox_tel_4";
            this.textBox_tel_4.ReadOnly = true;
            this.textBox_tel_4.Size = new System.Drawing.Size(235, 43);
            this.textBox_tel_4.TabIndex = 1;
            this.textBox_tel_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 339);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Mini_ATC";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button_main_cental;
        private System.Windows.Forms.Button button_main_down;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button button_12_tel_2;
        private System.Windows.Forms.Button button_0_tel_2;
        private System.Windows.Forms.Button button_11_tel_2;
        private System.Windows.Forms.Button button_9_tel_2;
        private System.Windows.Forms.Button button_8_tel_2;
        private System.Windows.Forms.Button button_7_tel_2;
        private System.Windows.Forms.Button button_6_tel_2;
        private System.Windows.Forms.Button button_5_tel_2;
        private System.Windows.Forms.Button button_4_tel_2;
        private System.Windows.Forms.Button button_3_tel_2;
        private System.Windows.Forms.Button button_2_tel_2;
        private System.Windows.Forms.Button button_1_tel_2;
        private System.Windows.Forms.Button button_central_tel_1;
        private System.Windows.Forms.Button button_12_tel_1;
        private System.Windows.Forms.Button button_1_tel_1;
        private System.Windows.Forms.Button button_0_tel_1;
        private System.Windows.Forms.Button button_11_tel_1;
        private System.Windows.Forms.Button button_9_tel_1;
        private System.Windows.Forms.Button button_8_tel_1;
        private System.Windows.Forms.Button button_7_tel_1;
        private System.Windows.Forms.Button button_6_tel_1;
        private System.Windows.Forms.Button button_5_tel_1;
        private System.Windows.Forms.Button button_4_tel_1;
        private System.Windows.Forms.Button button_3_tel_1;
        private System.Windows.Forms.Button button_2_tel_1;
        private System.Windows.Forms.Button button_central_tel_2;
        private System.Windows.Forms.Button button_central_tel_3;
        private System.Windows.Forms.Button button_central_tel_4;
        private System.Windows.Forms.Button button_12_tel_4;
        private System.Windows.Forms.Button button_0_tel_4;
        private System.Windows.Forms.Button button_11_tel_4;
        private System.Windows.Forms.Button button_9_tel_4;
        private System.Windows.Forms.Button button_8_tel_4;
        private System.Windows.Forms.Button button_7_tel_4;
        private System.Windows.Forms.Button button_6_tel_4;
        private System.Windows.Forms.Button button_5_tel_4;
        private System.Windows.Forms.Button button_4_tel_4;
        private System.Windows.Forms.Button button_3_tel_4;
        private System.Windows.Forms.Button button_2_tel_4;
        private System.Windows.Forms.Button button_1_tel_4;
        private System.Windows.Forms.Button button_12_tel_3;
        private System.Windows.Forms.Button button_11_tel_3;
        private System.Windows.Forms.Button button_9_tel_3;
        private System.Windows.Forms.Button button_8_tel_3;
        private System.Windows.Forms.Button button_7_tel_3;
        private System.Windows.Forms.Button button_6_tel_3;
        private System.Windows.Forms.Button button_5_tel_3;
        private System.Windows.Forms.Button button_4_tel_3;
        private System.Windows.Forms.Button button_3_tel_3;
        private System.Windows.Forms.Button button_2_tel_3;
        private System.Windows.Forms.Button button_1_tel_3;
        private System.Windows.Forms.Button button_0_tel_3;
        public System.Windows.Forms.Button button_main_call;
        public System.Windows.Forms.Button button_main_12;
        public System.Windows.Forms.Button button_main_0;
        public System.Windows.Forms.Button button_main_11;
        public System.Windows.Forms.Button button_main_9;
        public System.Windows.Forms.Button button_main_8;
        public System.Windows.Forms.Button button_main_7;
        public System.Windows.Forms.Button button_main_6;
        public System.Windows.Forms.Button button_main_5;
        public System.Windows.Forms.Button button_main_4;
        public System.Windows.Forms.Button button_main_3;
        public System.Windows.Forms.Button button_main_2;
        public System.Windows.Forms.Button button_main_1;
        public System.Windows.Forms.TextBox textBox_main;
        public System.Windows.Forms.TextBox textBox_tel_2;
        public System.Windows.Forms.TextBox textBox_tel_1;
        public System.Windows.Forms.TextBox textBox_tel_3;
        public System.Windows.Forms.TextBox textBox_tel_4;
        public System.Windows.Forms.Button button_down_tel_2;
        public System.Windows.Forms.Button button_call_tel_1;
        public System.Windows.Forms.Button button_down_tel_1;
        public System.Windows.Forms.Button button_call_tel_2;
        public System.Windows.Forms.Button button_down_tel_3;
        public System.Windows.Forms.Button button_call_tel_4;
        public System.Windows.Forms.Button button_down_tel_4;
        public System.Windows.Forms.Button button_call_tel_3;
        public System.Windows.Forms.Button button_main_backspace;
    }
}

